# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables＃＃＃##

- 'LISTEN_PORT' - port to listen on (default:'8000')
- 'APP_HOST' - Hostname of the app to forward requests to (defalt: 'app')
- 'APP_PORT' - Port of the app to woward requests to (default '9000')
